import java.util.List;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;


public class CleanUpJob extends Configured implements Tool {

	/*
	 * The cleanup job removes unnecessary outlinks and filters the top 100 nodes.
	 */
	
    List<String> toBeCleaned;
    String inPath;
    String outPath;
  
    public CleanUpJob(String inPath, String outputPath, List<String> toBeCleaned) {
        this.inPath = inPath;
        this.outPath = outputPath;
        this.toBeCleaned = toBeCleaned;
    }

    @Override
    public int run(String[] strings) throws Exception {
        Job cleanUpJob = Job.getInstance(getConf(), "init");
        cleanUpJob.setJarByClass(this.getClass());
        FileInputFormat.addInputPaths(cleanUpJob, inPath);
        FileOutputFormat.setOutputPath(cleanUpJob, new Path(outPath));
        cleanUpJob.setMapperClass(CleanUpMapper.class);
        cleanUpJob.setReducerClass(CleanUpReducer.class);
        cleanUpJob.setSortComparatorClass(MyComparator.class);
        cleanUpJob.setMapOutputKeyClass(DoubleWritable.class);
        cleanUpJob.setMapOutputValueClass(Text.class);
        cleanUpJob.setNumReduceTasks(1);  //To set no of reducers to 1
        cleanUpJob.waitForCompletion(true);

        boolean result = cleanUpJob.waitForCompletion(true);
        /*
         * To clean all intermediate directories.
         */
        
        FileSystem fs = FileSystem.get(getConf());  
        for (String path : toBeCleaned) {
            Path pth = new Path(path);
            fs.delete(pth, true);
        }
        return result ? 0 : 1;
    }

}


class MyComparator extends WritableComparator {

    protected MyComparator() {
        super(DoubleWritable.class, true);
    }

    /*
     *  Comparator will sort in descending order
     */
    
    @Override
    public int compare(WritableComparable a, WritableComparable b) {
        DoubleWritable key1 = (DoubleWritable) a;
        DoubleWritable key2 = (DoubleWritable) b;
        return -1 * key1.compareTo(key2);
    }

}