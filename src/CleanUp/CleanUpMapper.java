import java.io.IOException;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class CleanUpMapper extends Mapper<LongWritable, Text, DoubleWritable, Text> {

	//Mapper emits rank as key and page as value, so we can sort on rank.
	
    @Override
    protected void map(LongWritable key, Text value, Mapper.Context context) throws IOException, InterruptedException {
        
    	String[] val = value.toString().split("##Separator##");
        String page = val[0];
        val = val[1].split("#OutputDocId#");
        double rank = Double.parseDouble(val[0]);
        context.write(new DoubleWritable(rank), new Text(page));
    }
}