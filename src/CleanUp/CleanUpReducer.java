import java.io.IOException;
import java.util.Iterator;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class CleanUpReducer extends Reducer<DoubleWritable, Text, Text, DoubleWritable>  {
    
    //Reducer emits output pages that are sorted by their ranks.
    @Override
    protected void reduce(DoubleWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
   
            Iterator<Text> iterator = values.iterator();
            while(iterator.hasNext()){
                context.write(iterator.next(), key);
    }
 }    
}