import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;

public class CalcPageRankJob extends Configured implements Tool {

	String inPath;
    String outPath;

    public CalcPageRankJob(String inPath, String outPath) {
        this.inPath = inPath;
        this.outPath = outPath;
    }

    @Override
    public int run(String[] strings) throws Exception {
        Job calcPageRankJob;
        calcPageRankJob = Job.getInstance(getConf(), "ranker");
        calcPageRankJob.setJarByClass(this.getClass());
        PageRankDriver.setSeparator(calcPageRankJob.getConfiguration());
        FileInputFormat.addInputPaths(calcPageRankJob, inPath);
        FileOutputFormat.setOutputPath(calcPageRankJob, new Path(outPath));
        calcPageRankJob.setMapperClass(CalcPageRankMapper.class);
        calcPageRankJob.setReducerClass(CalcPageRankReducer.class);
        calcPageRankJob.setOutputFormatClass(TextOutputFormat.class);
        calcPageRankJob.setMapOutputKeyClass(Text.class);
        calcPageRankJob.setMapOutputValueClass(Text.class);
        return calcPageRankJob.waitForCompletion(true) ? 0 : 1;
    }

}