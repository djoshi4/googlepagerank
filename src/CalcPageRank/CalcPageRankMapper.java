import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


public class CalcPageRankMapper extends Mapper<LongWritable, Text, Text, Text> {

	/*
	 * Mapper calculates page rank for each outlink of the node and emits :
	 * 1. outlink and its rank
	 * 2. all outlinks of the node
	 */
	
    @Override
    protected void map(LongWritable key, Text value, Mapper.Context context) throws IOException, InterruptedException {
        
    	//Split the node(page) and details
        String[] data = value.toString().split(PageRankDriver.MRKeySeparator);
        String page = data[0];
        
        //Split the page rank and outlinks on separator
        data = data[1].split("#OutputDocId#");
        double rank = Double.parseDouble(data[0]);
        String outlinks = "";
        if (data.length > 1) {
            outlinks = data[1];
            data = data[1].split(PageRankDriver.ValueSeparator);
            double pageRank = rank / data.length;
            for (String d : data) {
                context.write(new Text(d), new Text("" + pageRank));
            }
        }      
        context.write(new Text(page), new Text(outlinks));
    }
}