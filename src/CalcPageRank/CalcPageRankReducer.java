
import java.io.IOException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


public class CalcPageRankReducer extends Reducer<Text, Text, Text, Text> {

	/*
	 * Reducer calculates the page rank for each node and emits node as key and <rank> valueSeparator <outlinks> as value 
	 * */
   
    	 @Override
    	    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
    	        
    	        String outlinks = "";
    	        double rank = 0;
    	      
    	        for (Text value : values) {
    	            if (value.toString().contains(PageRankDriver.ValueSeparator)) {
    	                outlinks = value.toString();
    	            } else if (!value.toString().equals("")) {
    	                rank += Double.parseDouble(value.toString());
    	            }
    	        }
    	        
    	        //Calculation of page rank along wth damping factor
    	        rank = .15 + .85 * rank;
    	        context.write(key, new Text(rank + "#OutputDocId#" + outlinks));
    	    }

 
}