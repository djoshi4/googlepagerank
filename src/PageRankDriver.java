import java.util.ArrayList;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.ToolRunner;


public class PageRankDriver {
	
    //Static variables declared to be shared between mappers and reducers.
	
	public static String ValueSeparator = "&&Separator&&";
    public static String MRKeySeparator = "##Separator##";
	
	//Function for setting separator.
    public static void setSeparator(Configuration conf) {
        conf.set("mapreduce.output.textoutputformat.separator", MRKeySeparator);
    }
    
    
    public static void main(String[] args) throws Exception {
    	
    	String userInput = args[0];
        String userOutput = args[1];
        ArrayList<String> tobeCleaned = new ArrayList<>();
                
        //To run the line count job.
        int nodes = ToolRunner.run(new CountLineJob(userInput, userInput + "_CountOfLine"), null);
        tobeCleaned.add(userInput + "_CountOfLine");

        //To run the job for graph creation.
        ToolRunner.run(new InitializeGraphJob(userInput, userInput + "Phase0", nodes), null);
        tobeCleaned.add(userInput + "Phase0");

        //To run the page rank job 10 times.
        for (int i = 1; i <= 10; i++) {
            ToolRunner.run(new CalcPageRankJob(userInput + "Phase" + (i - 1), userInput + "Phase" + i), null);
            tobeCleaned.add(userInput + "Phase" + i);
        }

        //To run the clean up job.
        ToolRunner.run(new CleanUpJob(userInput + "Phase10", userOutput, tobeCleaned), null);
    }
   
}