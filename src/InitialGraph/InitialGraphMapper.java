
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class InitialGraphMapper extends Mapper<LongWritable, Text, Text, Text> {

    private static final Pattern PATTERN = Pattern.compile("\\[\\[.*?\\]\\]");
    
    Pattern TITLE = Pattern.compile("<title>(.*?)</title>");

    // Map function emits page no and outlinks as key value pair
    
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

    	String title= null;
        String ln = value.toString();
        ln = ln.trim();
        
        List<String> outLinks = new ArrayList<>();
        Matcher m1 = TITLE.matcher(ln);
        Matcher m2 = PATTERN.matcher(ln);
        StringBuilder sbldr = new StringBuilder();
        
        //To extract the title
        if (m1.find()) {
            title = m1.group();
            title = title.trim();
            title = title.substring(7, title.length() - 8);
        } else {
            return;		//Return if there is no title
        }

        //To extract the outlinks
        while (m2.find()) {
            String outlink = m2.group();
            outlink = outlink.trim();
            outlink = outlink.substring(2, outlink.lastIndexOf(']') - 1);
            outlink = outlink.replace("[[", "");
            outLinks.add(outlink);
        }

        //To append all outlinks and form a string separated by a separator
        for (int i = 0; i < outLinks.size(); i++) {
            String data = outLinks.get(i);
            sbldr.append(data);
            if (i != outLinks.size() - 1) {
                sbldr.append(PageRankDriver.ValueSeparator);
            }
        }
        
        //Emit node and <rank> valueSeparator <list of outlinks> as key value pair
        context.write(new Text(title), new Text(sbldr.toString()));
    }

}