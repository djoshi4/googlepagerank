import java.io.IOException;
import java.util.Iterator;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


public class InitialGraphReducer extends Reducer<Text, Text, Text, Text> {

    double initRank;

    //Setup generates an initial rank
    
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context); 
        initRank = context.getConfiguration().getDouble("InputFiles", 0);
        initRank = 1.0 / initRank;
    }
    
    /*
     * Reduce job writes the rank along the ith output
     */
    
    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
    	
        Iterator<Text> itr = values.iterator();
        StringBuilder sb = new StringBuilder();
        
        // Append all outlinks of the page
        while (itr.hasNext()) {
            Text val = itr.next();
            sb.append(val.toString());
            sb.append(PageRankDriver.ValueSeparator);
        }
        
        //Emit page no, its rank and its outlinks
        context.write(key, new Text(initRank + "#OutputDocId#" + sb.toString()));
    }

}