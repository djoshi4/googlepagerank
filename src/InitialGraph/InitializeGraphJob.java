
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;

public class InitializeGraphJob extends Configured implements Tool {

	/*
	 * Graph initialization in the form: "page_no <valueSeparator> initial_rank <valueSeparator> outlinks" .
	 *  
	 */
	
    int countOfNodes;
    String outPath, inPath;
    
    public InitializeGraphJob(String inPath, String outPath, int countOfNode) {
        this.inPath = inPath;
        this.outPath = outPath;
        this.countOfNodes = countOfNode;
    }

    @Override
    public int run(String[] strings) throws Exception {
        Job graphJob = Job.getInstance(getConf(), "initMap");
        graphJob.setJarByClass(this.getClass());
        PageRankDriver.setSeparator(graphJob.getConfiguration());
        FileInputFormat.addInputPaths(graphJob, inPath);
        FileOutputFormat.setOutputPath(graphJob, new Path(outPath));
        graphJob.getConfiguration().set("InputFiles", "" + countOfNodes);
        graphJob.setMapperClass(InitialGraphMapper.class);
        graphJob.setReducerClass(InitialGraphReducer.class);
        graphJob.setMapOutputKeyClass(Text.class);
        graphJob.setMapOutputValueClass(Text.class);
        graphJob.setOutputFormatClass(TextOutputFormat.class);
        
        graphJob.waitForCompletion(true);
        return graphJob.waitForCompletion(true) ? 0 : 1;
    }
}