import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class CountLineReducer extends Reducer<Text, IntWritable, Text, IntWritable> {

	/* Reduce job increments the count for each node(page)
	 */
    int cnt = 0;

    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        this.cnt++;
    }

    //Increment counter by no of nodes
    protected void cleanup(Context context) throws IOException, InterruptedException {
        context.getCounter("Result", "Result").increment(cnt);
    }

}