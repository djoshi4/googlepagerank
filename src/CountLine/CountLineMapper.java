import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class CountLineMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

	private static final Pattern PATTERN = Pattern.compile("\\[\\[.*?\\]\\]");
    
    Pattern TITLE = Pattern.compile("<title>(.*?)</title>");

    //Mapper emits 1 for every line in the file
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

    	String title = null;
        String ln = value.toString();
        ln = ln.trim();
       
        
        Matcher m1 = TITLE.matcher(ln);
        Matcher m2 = PATTERN.matcher(ln);

        //To find the page(node) in the title and emit it
        if (m1.find()) {
            title = m1.group();
            title = title.trim();
            title = title.substring(7, title.length() - 8);
            context.write(new Text(title), one);
        }
        
        //To find page(node) in text and emit it
        while (m2.find()) {
            String outlink = m2.group();
            outlink = outlink.trim();
            outlink = outlink.substring(2, outlink.lastIndexOf(']') - 1);
            context.write(new Text(outlink), one);
        }
    }

    private final static IntWritable one = new IntWritable(1);

}