
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;


public class CountLineJob extends Configured implements Tool {

    String inPath;
    String outPath;

    public CountLineJob(String inPath, String outPath) {
        this.inPath = inPath;
        this.outPath = outPath;
    }

    @Override
    public int run(String[] strings) throws Exception {
        Job countLineJob = Job.getInstance(getConf(), "linecount");
        countLineJob.setJarByClass(this.getClass());
        PageRankDriver.setSeparator(countLineJob.getConfiguration());
        FileInputFormat.addInputPaths(countLineJob, inPath);
        FileOutputFormat.setOutputPath(countLineJob, new Path(outPath));
        countLineJob.setMapperClass(CountLineMapper.class);
        countLineJob.setReducerClass(CountLineReducer.class);
        countLineJob.setMapOutputKeyClass(Text.class);
        countLineJob.setMapOutputValueClass(IntWritable.class);
        countLineJob.setOutputFormatClass(TextOutputFormat.class);
        countLineJob.setNumReduceTasks(1); //To set no of reducers to 1
        countLineJob.waitForCompletion(true);
        
        int count = (int) countLineJob.getCounters().findCounter("Result", "Result").getValue();  //To find total no of nodes
        System.out.println("\n\n\n Total no of nodes is: " + count + "\n\n");
        return count;
    }

}
